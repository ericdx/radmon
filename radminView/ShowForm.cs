﻿/*
 * Created by SharpDevelop.
 * User: Эрнест
 * Date: 29.06.2019
 * Time: 14:18
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */

using System;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Windows.Forms;

namespace radminView
{
    /// <summary>
    /// Description of ShowForm.
    /// </summary>
    public partial class adminView : Form
    {
        NotifyIcon icn;
        public adminView(NotifyIcon _icon)
        {
            //
            // The InitializeComponent() call is required for Windows Forms designer support.
            //
            InitializeComponent();
            timer1.Start();
            //refr();
            icn = _icon;
        }
        static bool isAlerted;
        void refresh()
        {
            var ip = System.Net.NetworkInformation.IPGlobalProperties
                .GetIPGlobalProperties();
            lvLog.Items.Clear();
            if (!ip.GetActiveTcpConnections().Any(c => c.LocalEndPoint.Port == 4899))
            { isAlerted = false; icn.Icon = new Icon("settings-128.ico"); }
            foreach (var tcp in ip.GetActiveTcpConnections())
            {
                if (tcp.LocalEndPoint.Port == 4899)
                    if (!isAlerted)
                    {
                        isAlerted = true;
                        // Notifier.alertMesg($"connected {tcp.RemoteEndPoint.Address}", "activity 4899");
                        icn.BalloonTipTitle = "Alarm";
                        icn.BalloonTipText = "I see!";
                        icn.ShowBalloonTip(9995000);
                        icn.Icon = SystemIcons.Warning;                       
                    }
                if (tcp.RemoteEndPoint.Port != 80 && tcp.RemoteEndPoint.Port != 443
                    && !IPAddress.IsLoopback(tcp.LocalEndPoint.Address)
                    )
                {
                    lvLog.Items.Add(
                        String.Format(
                           "{0} : {1} - {2} {3}",
                           tcp.LocalEndPoint.Address, tcp.State,
                           tcp.RemoteEndPoint.Address, tcp.RemoteEndPoint.Port));
                }
            }
        }

        void Timer1Tick(object sender, EventArgs e)
        {
            refresh();
        }

        void AdminViewShown(object sender, EventArgs e)
        {
            Hide();
        }

        private void adminView_FormClosing(object sender, FormClosingEventArgs e)
        {
            Hide();
        }
    }
}
