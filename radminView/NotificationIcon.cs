﻿/*
 * Created by SharpDevelop.
 * User: Эрнест
 * Date: 29.06.2019
 * Time: 14:16
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Diagnostics;
using System.Drawing;
using System.Threading;
using System.Windows.Forms;

namespace radminView
{
	public class NotificationIcon
	{
		private NotifyIcon notifyIcon;
		private ContextMenu notificationMenu;
		
		#region Initialize icon and menu
		public NotificationIcon()
		{
			notifyIcon = new NotifyIcon();
			notificationMenu = new ContextMenu(InitializeMenu());
			
			notifyIcon.Click += IconClick;
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(NotificationIcon));
            notifyIcon.Icon = new Icon("settings-128.ico"); //SystemIcons.Application;//(Icon)resources.GetObject("$this.Icon");
			notifyIcon.ContextMenu = notificationMenu;
			notifyIcon.Visible = true;
			notifyIcon.BalloonTipTitle = "Alarm";
			notifyIcon.BalloonTipText = "Hi!";
			notifyIcon.ShowBalloonTip(5000);
			
		}
		
		private MenuItem[] InitializeMenu()
		{
			MenuItem[] menu = new MenuItem[] {
				new MenuItem("About", menuAboutClick),
				new MenuItem("Exit", menuExitClick),
			//	new MenuItem("Show", menuShowClick),
			};
			return menu;
		}
		#endregion
		static NotificationIcon notificationIcon = new NotificationIcon();
		#region Main - Program entry point
		/// <summary>Program entry point.</summary>
		/// <param name="args">Command Line Arguments</param>
		[STAThread]
		public static void Main(string[] args)
		{
			Application.EnableVisualStyles();
			//Application.SetCompatibleTextRenderingDefault(false);
			
			bool isFirstInstance;
			// Please use a unique name for the mutex to prevent conflicts with other programs
			using (Mutex mtx = new Mutex(true, "radminView", out isFirstInstance)) {
				if (isFirstInstance) {					
					notificationIcon.notifyIcon.Visible = true;					
					Application.Run(adm);
					notificationIcon.notifyIcon.Dispose();
				} else {
					// The application is already running
					// TODO: Display message box or change focus to existing application instance
				}
			} // releases the Mutex
		}
		#endregion
		static adminView adm = new adminView(notificationIcon.notifyIcon);
		#region Event Handlers
		private void menuAboutClick(object sender, EventArgs e)
		{
			MessageBox.Show("About This Application");
		}
		
		private void menuExitClick(object sender, EventArgs e)
		{
			Application.Exit();
		}
		private void menuShowClick(object sender, EventArgs e)
		{
			//var frm = new adminView();
		}
		
		private void IconClick(object sender, EventArgs e)
		{            	
            if (adm.Visible) adm.Hide();
            else
            {
                adm.Show(); adm.WindowState = FormWindowState.Normal;
            }
		}
		#endregion
	}
}
